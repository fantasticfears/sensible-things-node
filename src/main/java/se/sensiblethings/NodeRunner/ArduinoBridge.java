package se.sensiblethings.NodeRunner;

import org.ardulink.core.Link;
import org.ardulink.core.Pin;
import org.ardulink.core.linkmanager.LinkManager;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import static org.ardulink.core.convenience.Links.setChoiceValues;

/**
 * Created by fantasticfears on 28/04/16.
 */
public class ArduinoBridge {
    @Option(name = "-delay", usage = "Do a n seconds delay after connecting")
    private int sleepSecs = 2;

    @Option(name = "-v", usage = "Be verbose")
    private boolean verbose = true;

    @Option(name = "-connection", usage = "Connection URI to the arduino")
    private String connString = "ardulink://serial?qos=true";

    @Option(name = "-d", aliases = "--digital", usage = "Digital pins to listen to")
    private int[] digitals = new int[] { 2 };

    @Option(name = "-a", aliases = "--analog", usage = "Analog pins to listen to")
    private int[] analogs = new int[0];

    @Option(name = "-msga", aliases = "--analogMessage", usage = "Message format for analog pins")
    private String msgAnalog = "PIN state changed. Analog PIN: %s Value: %s";

    @Option(name = "-msgd", aliases = "--digitalMessage", usage = "Message format for digital pins")
    private String msgDigital = "PIN state changed. Digital PIN: %s Value: %s";
    private static final Logger logger = LoggerFactory
            .getLogger(ArduinoBridge.class);


    private Link createLink() throws Exception, URISyntaxException {
        LinkManager.Configurer configurer = setChoiceValues(
                LinkManager.getInstance().getConfigurer(new URI(connString)));
        System.out.println(configurer.getAttribute("qos").getValue());
        return configurer.newLink();
    }
}
