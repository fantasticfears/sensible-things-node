package se.sensiblethings.NodeRunner; /**
 * Created by fantasticfears on 06/04/16.
 */

import org.ardulink.core.Link;
import org.ardulink.core.Pin;
import org.ardulink.core.convenience.Links;
import org.ardulink.core.events.AnalogPinValueChangedEvent;
import org.ardulink.core.events.DigitalPinValueChangedEvent;
import org.ardulink.core.events.EventListener;
import org.ardulink.core.linkmanager.LinkManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.sensiblethings.interfacelayer.SensibleThingsListener;
import se.sensiblethings.interfacelayer.SensibleThingsNode;
import se.sensiblethings.interfacelayer.SensibleThingsPlatform;
import org.kohsuke.args4j.Option;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static org.ardulink.core.convenience.Links.setChoiceValues;

public class SourceNode implements SensibleThingsListener {

    //SensibleThings Platform Application Interface
    SensibleThingsPlatform platform = null;
    Link link = null;

    long duration1s = 1000000000L;
    long duration10s = 10 * duration1s;
    long duration120s = duration1s * 120;

    long worldStartTime = System.nanoTime();
    long startWatchTime = worldStartTime + duration10s;
    long endWatchTime = startWatchTime + duration120s;

    int receivedMessage = 0;
    int sentMessage = 0;

    @Option(name = "-connection", usage = "Connection URI to the arduino")
    private String connString = "ardulink://serial"; //"ardulink://serial?qos=true";

    @Option(name = "-msga", aliases = "--analogMessage", usage = "Message format for analog pins")
    private String msgAnalog = "PIN state changed. Analog PIN: %s Value: %s";

    @Option(name = "-msgd", aliases = "--digitalMessage", usage = "Message format for digital pins")
    private String msgDigital = "PIN state changed. Digital PIN: %s Value: %s";

    private static final Logger logger = LoggerFactory
            .getLogger(SourceNode.class);

    public void run(){
        logger.info("started");
        // disable for unusable bootstrap
        // platform = new SensibleThingsPlatform(this);


        try {
            final Pin.DigitalPin pin = Pin.digitalPin(9);
            final Pin.AnalogPin analogPin = Pin.analogPin(0);

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run(){
                    try {
                        boolean power = true;
                        while (true) {
                            long timeNow = System.nanoTime();
                            if (timeNow - startWatchTime > 0) {
                                if (timeNow - endWatchTime > 0) {
                                    outputAndShutDown();
                                }
                            }

                            link.switchDigitalPin(pin, power);
                            sentMessage += 1;
                            power = !power;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            worldStartTime = System.nanoTime();
            startWatchTime = worldStartTime + duration10s;
            endWatchTime = startWatchTime + duration120s;
            logger.info("startTime: " + worldStartTime);
            logger.info("startWatchTime: " + startWatchTime);
            logger.info("endWatchTime: " + endWatchTime);

            this.link = createLink();
            link.addListener(eventListener());

            link.startListening(analogPin);
            thread.run();

        } catch (Exception e) {
            e.printStackTrace();
        }



//        logger.debug("run");
//        try {
//            this.link = createLink();
//            logger.debug("link created");
//
//            Pin.DigitalPin pin = Pin.digitalPin(9);
//            boolean power = true;
//            while (true) {
//                System.out.println("Send power:" + power);
//                link.switchDigitalPin(pin, power);
//                power = !power;
//                TimeUnit.SECONDS.sleep(2);
//            }
//
////            platform.resolve("fantasticfears@student.miun.se/sensor");
////
////            //Register and publishing the sensor UCI
////            platform.register("fantasticfears@student.miun.se/sensor");
////            platform.register("fantasticfears@student.miun.se/sensor2");
////
////            platform.resolve("fantasticfears@student.miun.se/sensor");
////            platform.resolve("fantasticfears@student.miun.se/sensor2");
////            platform.resolve("fantasticfears@student.miun.se/sink");
////            platform.resolve("bootstrap@miun.se/random");
//
//            //Run in background until closed down
////            System.out.println("SensibleThings SourceExample is running");
////            System.out.println("\nPress any key to shutdown");
////            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
////            in.readLine();
////
////            //Shutdown all background tasks
////            platform.shutdown();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public void outputAndShutDown() {
        System.out.println("send: " + sentMessage);
        System.out.println("recv: " + receivedMessage);
        System.exit(0);
    }

    @Override
    public void getEvent(SensibleThingsNode source, String uci) {

        //This is called on the source as a response the Get function call from a sink
        //The idea is that this function shall answer back with the newest value from the sensors
        System.out.println("[GetEvent] " + source + " : " + uci);

        //We send back our simulated sensor value
//        platform.notify(source, uci, simulatedSensorValue);
    }

    @Override
    public void setEvent(SensibleThingsNode source, String uci, String value) {
        //This is called on the source as a response the Set function call from another node
        //The idea is that this function shall set an actuator.
        System.out.println("[SetEvent] " + source + " : " + uci + " : " + value);

        //In this example we set out simulated sensor value
//        simulatedSensorValue = value;
    }

    @Override
    public void getResponse(String uci, String value, SensibleThingsNode fromNode) {
        System.out.println("[GetEvent] " + uci + " : " + " : " + fromNode);
    }

    @Override
    public void resolveResponse(String uci, SensibleThingsNode node) {
        System.out.println("[ResolveResponse] " + uci + ": " + node);
    }

    private EventListener eventListener() {
        return new EventListener() {
            @Override
            public void stateChanged(DigitalPinValueChangedEvent event) {
                logger.info(format(msgDigital, event.getPin(), event.getValue()));
            }

            @Override
            public void stateChanged(AnalogPinValueChangedEvent event) {
                long timeNow = System.nanoTime();
                if (timeNow - startWatchTime > 0) {
                    if (timeNow - endWatchTime > 0) {
                        outputAndShutDown();
                    }
                }

                receivedMessage += 1;
//                logger.info(format(msgAnalog, event.getPin(), event.getValue()));
            }
        };
    }

    private Link createLink() throws Exception, URISyntaxException {
        LinkManager.Configurer configurer = setChoiceValues(
                LinkManager.getInstance().getConfigurer(new URI(connString)));
        System.out.println(configurer.getAttribute("qos").getValue());
        return configurer.newLink();
    }
}