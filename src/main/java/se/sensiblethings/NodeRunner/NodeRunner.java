/**
 * Created by fantasticfears on 06/04/16.
 */

package se.sensiblethings.NodeRunner;

public class NodeRunner {

    // Nodes running on laptop simplified deployment overhead.
    // however, it has to be threaded
    public static final void main(String[] args) {
        try {
            System.out.println("Main In");
            SourceNode node = new SourceNode();
            System.out.println("Node is going to run");
            node.run();
            // new ArduinoBridge().run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
